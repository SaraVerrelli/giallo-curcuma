<x-layout>
    <main class="container-fluid">
        <h1 class="my-3 text-center">Aggiungi una nuova ricetta</h1>
          <div class="row">
              @if (session('message'))
              <div class="alert alert-success">
                  {{session('message')}}
              </div>
              
              @endif
          </div>
        <div class="row mx-auto">
            <div class="col-12 col-sm-6 offset-sm-3">
                <form method="POST" action="{{route('storeRecipe')}}">
                    @csrf
                    <div class="mb-3">
                      <label class="form-label">Nome</label>
                      <input type="text" class="form-control" name="name">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Ingredienti</label>
                        <select name="ingredients[]" multiple class="form-select" aria-label="Default select example">
                          @foreach ($ingredients as $ingredient)
                          <option value="{{$ingredient->id}}">{{$ingredient->name}}</option>                             
                          @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Procedimento</label>
                        <textarea cols="30" rows="10" class="form-control" name="description"></textarea>
                    </div>
                    <div class="mb-3">
                      <label class="form-label">Difficoltà</label>
                      <select name="difficolta" class="form-select">
                        <option value="facile">Facile</option>                          
                        <option value="media">Media</option>                          
                        <option value="difficile">Difficile</option>    
                      </select>
                  </div>
                  <div class="mb-3">
                    <label class="form-label">Tempo</label>
                    <select name="tempo" class="form-select">
                      <option value="veloce">Veloce</option>                          
                      <option value="medio">Medio</option>                          
                      <option value="lungo">Lungo</option>    
                    </select>
                </div>
                   
                    <div class="mb-3 form-check">
                      <input type="checkbox" class="form-check-input" name="vegan" value="vegan">
                      <label class="form-check-label">Vegano</label>
                    </div>
                    <div class="mb-3 form-check">
                      <input type="checkbox" class="form-check-input" name="vegetarian" value="vegetarian">
                      <label class="form-check-label">Vegetariano</label>
                    </div>
                    <div class="mb-3 form-check">
                      <input type="checkbox" class="form-check-input" name="lactose" value="lactose">
                      <label class="form-check-label">Lattosio</label>
                    </div>
                    <div class="mb-3 form-check">
                      <input type="checkbox" class="form-check-input" name="gluten" value="gluten">
                      <label class="form-check-label">Glutine</label>
                    </div>
                    <button type="submit" class="btn btn-primary">Aggiungi</button>
                  </form>
            </x-layout>
            </div>
        </div>
    </main>

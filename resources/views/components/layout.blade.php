<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Giallo</title>
  
  <link rel="stylesheet" href="{{asset('css/app.css')}}">
  
  <script src="https://kit.fontawesome.com/2bab0aa439.js" crossorigin="anonymous"></script>
  <script src="{{asset('js/app.js')}}"></script>
</head>
<body class="bg-light">
  <nav class="navbar navbar-expand-md navbar-light bg-orange">
    <div class="container-fluid">
      <a class="navbar-brand cl-violet" href="{{route('home')}}"><strong>Giallo Curcuma</strong><i class="ms-2 cl-violet fas fa-2x fa-utensils"></i></a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <ul class="nav nav-pills nav-fill ms-auto">
          <li class="nav-item">
            <a class="nav-link text-dark" href="{{route('home')}}">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-dark" href="{{route('allIngredients')}}">I nostri ingredienti</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-dark" href="{{route('allRecipes')}}">Le nostre ricette</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-dark" href="{{route('searchForm')}}">Cerca</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-dark" href="{{route('ingredientForm')}}">Aggiungi ingrediente</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-dark" href="{{route('recipeForm')}}">Aggiungi ricetta</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  
  {{$slot}}
</body>
</html>
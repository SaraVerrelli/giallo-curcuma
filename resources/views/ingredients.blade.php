<x-layout>
    <main class="container-fluid">
        <h1 class="my-3 text-center">Aggiungi un nuovo ingrediente</h1>
        <div class="row">
            @if (session('message'))
            <div class="alert alert-success">
                {{session('message')}}
            </div>
            
            @endif
        </div>
        <div class="row mx-auto">
            <div class="col-12 col-sm-6 offset-sm-3">
                <form method="POST" action="{{route('storeIngredient')}}">
                @csrf
                <div class="mb-3">
                <label class="form-label">Ingrediente</label>
                <input type="text" class="form-control" name="name">
                </div>
                <div class="mb-3">
                    <label class="form-label">Unità di misura</label>
                    <select name="measure" id="" class="form-control">
                        <option value="Kg">Kg</option>
                        <option value="g">g</option>
                        <option value="ml">ml</option>
                        <option value="pizzico">pizzico</option>
                        <option value="pezzo">pezzo</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Aggiungi</button>
            </form>
            </div>
        </div>
    </main>
</x-layout>
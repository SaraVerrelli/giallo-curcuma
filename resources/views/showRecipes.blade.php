<x-layout>
    <main class="container-fluid">
        <div class="row mx-auto">
            @foreach ($recipes as $recipe)
            <div class="col-12 col-sm-4 col-md-3 mt-3">
                <div class="card">
                    <div class="card-body">
                      <h5 class="card-title">{{$recipe->name}}</h5>
                      <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                      <a href="#" class="btn btn-primary">Vedi</a>
                    </div>
                  </div>
            </div>   
            @endforeach
           
        </div>
    </main>
</x-layout>
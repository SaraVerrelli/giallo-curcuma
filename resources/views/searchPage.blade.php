<x-layout>
    <main class="container-fluid">
        <div class="row mx-auto">
            <form class="d-flex" action="{{route('searchByIngredients')}}" method="POST">
                @csrf
                <div class="mb-3">
                     <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit">Search</button>
                </div>
               
                <div class="mb-3">
                    <label class="form-label">Ingredienti</label>
                    <select name="ingredients[]" multiple class="form-select" aria-label="Default select example">
                      @foreach ($ingredients as $ingredient)
                      <option value="{{$ingredient->id}}">{{$ingredient->name}}</option>                             
                      @endforeach
                    </select>
                </div>
                <div class="mb-3">
                    <button type="submit" class="btn btn-primary">Cerca</button>
                </div>
              </form>
        </div>
    </main>
</x-layout>
<x-layout>
    <main class="container-fluid">
        <div class="row">
            @if (session('message'))
            <div class="alert alert-success">
                {{session('message')}}
            </div>
            
            @endif
        </div>
        <div class="row mx-auto">
            @foreach ($ingredients as $ingredient)
            <div class="col-6 col-sm-4 col-md-2 mt-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">{{$ingredient->name}}</h5>
                        <p class="text-end">{{$ingredient->measure}}</p>
                        <form method="POST" action="{{route('deleteIngredient', compact('ingredient'))}}">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-danger ms-3">Elimina</button>
                        </form>                    
                    </div>
                </div>
            </div>
            @endforeach
            
        </div>
    </main>
</x-layout>
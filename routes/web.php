<?php

use App\Http\Controllers\PublicController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'home'])->name('home');
Route::get('/nuova.ricetta', [PublicController::class, 'addRecipe'])->name('recipeForm');
Route::get('/nuovo.ingrediente', [PublicController::class, 'addIngredient'])->name('ingredientForm');
Route::post('/ricetta/store', [PublicController::class, 'storeRecipe'])->name('storeRecipe');
Route::post('/ingrediente/store', [PublicController::class, 'storeIngredient'])->name('storeIngredient');
Route::get('/ingredienti', [PublicController::class, 'showAllIngredients'])->name('allIngredients');
Route::get('/ricette', [PublicController::class, 'showAllRecipes'])->name('allRecipes');
Route::delete('/ingredienti/delete/{ingredient}', [PublicController::class, 'destroy'])->name('deleteIngredient');
Route::get('/cerca', [PublicController::class, 'searchForm'])->name('searchForm');
Route::post('/ricette/trova', [PublicController::class, 'searchRecipesByIngredients'])->name('searchByIngredients');

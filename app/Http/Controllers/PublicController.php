<?php

namespace App\Http\Controllers;

use App\Models\Ingredient;
use App\Models\Recipe;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function home()
    {
        return view('home');
    }
    public function addRecipe()
    {
        $ingredients = Ingredient::all();
        return view('recipes', compact('ingredients'));
    }
    public function addIngredient()
    {
        return view('ingredients');
    }

    public function storeRecipe(Request $request, Recipe $recipe)
    {
        $recipe = Recipe::create([
            'name' => $request->name,
            'description' => $request->description,
            'difficolta' => $request->difficolta,
            'tempo' => $request->tempo,
            'vegan' => $request->vegan,
            'vegetarian' => $request->vegetarian,
            'lactose' => $request->lactose,
            'gluten' => $request->gluten,

        ]);
        foreach ($request->ingredients as $ingredient) {
            $recipe->ingredients()->attach($ingredient);
        }
        return redirect(route('recipeForm'))->with('message', 'La tua ricetta è stata salvata');
    }

    public function storeIngredient(Request $request)
    {
        $ingredients = Ingredient::create([
            'name' => $request->name,
            'measure' => $request->measure
        ]);

        return redirect(route('ingredientForm'))->with('message', 'Il tuo ingrediente è stato salvato');
    }

    public function showAllIngredients()
    {
        $ingredients = Ingredient::all();
        return view('showIngredients', compact('ingredients'));
    }

    public function showAllRecipes()
    {
        $recipes = Recipe::all();
        return view('showRecipes', compact('recipes'));
    }

    public function destroy(Ingredient $ingredient)
    {
        $ingredient->recipes()->detach();
        $ingredient->delete();
        return redirect(route('allIngredients'))->with('message', 'Ingrediente eliminato');
    }

    public function searchForm()
    {
        $ingredients = Ingredient::all();
        return view('searchPage', compact('ingredients'));
    }

    public function searchRecipesByIngredients(Request $request)
    {
        $ingredients = Ingredient::find($request->ingredients)->foreach ($ingredients as $ingredient) {
            $ingredient->recipes()->get();
        } 
        
        // $ingredients = Ingredient::find($request->ingredients);

        // for ($i=0; $i<sizeof($ingredients) ; $i++) { 

        // }
    }
}

<?php

namespace App\Models;

use App\Models\Ingredient;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Recipe extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'description', 'difficolta', 'tempo', 'vegan', 'vegetarian', 'lactose', 'gluten'
    ];


    public function ingredients()
    {
        return $this->belongsToMany(Ingredient::class);
    }
}

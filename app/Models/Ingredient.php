<?php

namespace App\Models;

use App\Models\Recipe;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Ingredient extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'measure',
    ];



    public function recipes()
    {
        return $this->belongsToMany(Recipe::class);
    }
}
